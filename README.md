# ng-protractor #

This is an example application for testing with protractor


Once you clone the repository you have to execute inside the project:

* $> bower install
* $> live-server

If you don't have any of these tools you have to execute:

* $> npm install -g bower
* $> npm install -g live-server