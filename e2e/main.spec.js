describe('E2E: main page', function(){

  it('should exists calc div', function(){
    browser.get('http://127.0.0.1:8080/')
    var ele = by.id('calc')
    expect(browser.isElementPresent(ele)).toBe(true)
    element(by.model('calc.a')).sendKeys(2)
    element(by.model('calc.b')).sendKeys(4)
    element(by.id('suma')).click()
    expect(element(by.binding('calc.c')).getText()).toEqual('***6***')
  })

})
