(function() {
  'use strict';

  angular
    .module('app.calc')
    .controller('CalcController', CalcController)

  CalcController.$inject = ['CalcService']
  function CalcController(CalcService){
    var vm = this
    vm.suma = suma

    function suma(){
      vm.c = CalcService.suma(vm.a, vm.b)
    }
  }

}());
