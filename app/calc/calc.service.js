(function() {
  'use strict';

  angular
    .module('app.calc')
    .service('CalcService', CalcService)

    function CalcService(){
      this.suma = suma

      function suma(a, b){
        return parseInt(a) + parseInt(b)
      }
    }

}());
