(function() {
  'use strict';

  angular
    .module('app.filters')
    .filter('decorator', DecoratorFilter)

  function DecoratorFilter(){
    return function(input, chars){
      return chars + input + chars
    }
  }

}());
