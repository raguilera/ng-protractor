(function() {
  'use strict';

  describe('Decorator filter', function(){

    var filter

    beforeEach(function(){
      module('app.filters')

      inject(function(decoratorFilter){
          filter = decoratorFilter
      })

    })

    it('should decorate input with chars', function(){
      var input = 'test'
      var chars = '**'
      var expected = '**test**'
      var actual = filter(input, chars)
      expect(expected).toEqual(actual)
    })
  })

}());
