(function() {
  'use strict';

  describe('Calc Service', function(){

    var calcService

    beforeEach(function(){
      module('app.calc')

      inject(function(CalcService){
        calcService = CalcService
      })

    })

    it ('should sumar 2 and 1 and result 3', function(){
      var a = 2
      var b = 1
      var c = 3
      c = calcService.suma(a,b)
      expect(c).toEqual(3)
    })
  })

}());
