(function() {
  'use strict';

  describe('Calc Controller', function(){

    var controller, scope, calcService

    beforeEach(function(){
      module('app.calc')

      inject(function($controller, $rootScope, CalcService){
        controller = $controller
        scope = $rootScope
        calcService = CalcService
      })

    })

    it('suma dos numeros', function(){
      controller('CalcController as calc', {'$scope':scope, 'CalcService':calcService})
      scope.calc.a = 2
      scope.calc.b = 4
      scope.calc.suma()
      expect(scope.calc.c).toEqual(6)
    })
  })

}());
